import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Material App',
      home: Scaffold(
        body: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(top: 30, bottom: 30, left: 20),
              color: Color(0xff087F49),
              child: Row(
                children: <Widget>[
                  Container(
                    width: 50,
                    height: 50,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(
                        image: AssetImage("assets/logo.png")
                      )
                    ),
                  ),
                  SizedBox(width: 15,),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text('Good morning, Alice', style: TextStyle(color: Colors.white),),
                      Text('Get your favorite things for free!', style: TextStyle(color: Colors.white),),
                    ],
                  )
                ],
              ),
            ),
            Expanded(
              child: ListView(
                padding: EdgeInsets.all(5),
                scrollDirection: Axis.vertical,
                children: <Widget>[
                  buildRow("Iced Skinny Flavored Latte", "10", "assets/1.png"),
                  buildRow("Iced Flavored Latte", "15", "assets/2.png"),
                  buildRow("Iced Caramel Macchiato", "20", "assets/3.png"),
                  buildRow("Espresso Macchiato", "40", "assets/4.png"),
                  buildRow("Caramel Macchiato", "30", "assets/5.png"),
                  buildRow("Cappuccino", "5", "assets/6.png"),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
              color: Colors.white,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Icon(Icons.local_florist, color: Color(0xff007d44),),
                  Container(
                    padding: EdgeInsets.only(left: 5, top: 10, bottom: 10),
                    width: 120,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30),
                      color: Colors.red.withOpacity(.2)
                    ),
                    child: Row(
                      children: <Widget>[
                        SizedBox(width: 5,),
                        Icon(Icons.card_giftcard, color: Colors.red,),
                        SizedBox(width: 5,),
                        Text("Ưu đãi", style: TextStyle(color: Colors.red, fontWeight: FontWeight.bold),)
                      ],
                    ),
                  ),
                  Icon(Icons.person, color: Color(0xff007d44),),
                  Icon(Icons.map, color: Color(0xff007d44),),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

class buildRow extends StatelessWidget {
  final String name;
  final String percent;
  final String imgPath;

  const buildRow(this.name, this.percent, this.imgPath);
  @override

  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 7),
      child: Container(
        decoration: BoxDecoration(
          color: Color(0xffd5e8e2),
          borderRadius: BorderRadius.circular(5),
          border: Border.all(
            color: Colors.grey.withOpacity(.5),
            width: .5
          )
        ),
        padding: EdgeInsets.only(left: 15, right: 20,top: 15),
        child:Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(name, style: TextStyle(color: Color(0xff3e534c), fontStyle: FontStyle.italic, fontSize: 16),),
                Container(
                  width: 45,
                  height: 45,
                  decoration: BoxDecoration(
                      color: Color(0xff1e3733),
                      shape: BoxShape.circle,
                      border:Border.all(
                          color: Colors.white,
                          width: 3
                      )
                  ),
                  child: Center(child: Text(percent +'%' , style: TextStyle(color: Colors.white, fontSize: 14, fontWeight: FontWeight.w600),)),
                )
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Image.asset(imgPath, height: 100,),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    height: 40,
                    decoration: BoxDecoration(
                      color: Color(0xdd008944),
                      borderRadius: BorderRadius.circular(20)
                    ),
                    child: Center(
                      child: Text("Learn more", style: TextStyle(color: Colors.white),),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
